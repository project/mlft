Multi-Lingual Feedback Tab (MLFT) is a Free fully functional and easy to 
customize feedback tab by One Hour Translation.
MLFT allows any site owner to add multi-lingual feedback tab to any page in 
a simple and easy way.

Main Features:

    * Add multi-lingual feedback to any web-page easily
    * Free of charge
    * Multi Lingual - users can see the tab and provide the feedback in their 
	  language
    * Dynamic language selection - controlled by the admin
    * Users Feedback is sent by email and is also available online
    * Feedback is machine translated with Google and can be human translated by 
	  One Hour Translation.
    * Fields selection - admin has full control over the tab look and feel
    * Tab skins - Admin can fully customize the tab with any background you like
    * Generate as many tabs as you like
    * Complete Feedback management system

To use the Multi Lingual Feedback Tab:

    * Download, install and activate the module
    * Register to One Hour Translation (free) to open an account. The account 
	  will be used to manage the tab/s.
    * Goto https://www.onehourtranslation.com/mlf/myForms
    * Click "Create New Tab"
    * Enter Tab details and click Save.
    * Copy the Formid from the tab "Get Code" section.
    * Paste the Formid to the configuration section on the Drupal module and 
	  click "save configuration
